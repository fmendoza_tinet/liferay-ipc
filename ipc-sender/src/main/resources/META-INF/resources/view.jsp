<%@ page language="java" contentType="text/html; charset=UTF-8" %>
 
<%@ include file="./init.jsp" %>

<portlet:actionURL name="addEvento" var="addEventoUrl"/>
<portlet:actionURL name="otroEvento" var="otroEventoUrl"/>
<portlet:actionURL name="enviarSesion" var="enviarSesionUrl"/>

<aui:form action="${addEventoUrl}">
    <aui:input name="campoTexto" type="text" label="Texto a enviar" />
    <aui:button name="addButton" type="button" value="Enviar via POST" />
	<aui:button name="addButtonJS" type="button" value="Enviar via JS" />
	<aui:button name="addButtonSesion" type="button" value="Enviar via Sesion" />
    <aui:button name="addButtonEvent" type="button" value="Enviar otro evento" />
</aui:form>


<aui:script use="aui-base,aui-request">

	var miformulario = AUI().one('#<portlet:namespace />fm');

	// Envio IPC via evento.
	// Se debe hacer por POST para que se refresque el otro formulario.
	miformulario.one('#<portlet:namespace />addButton').on('click', function(event) {
		console.log("Enviando formulario...");
		miformulario.attr('action', '${addEventoUrl}');
		miformulario.submit();
	});

	// Envio IPC via JS
	miformulario.one('#<portlet:namespace />addButtonJS').on('click', function(event) {
		console.log("Enviar datos al otro portlet");
		//recuperar texto
		nombre = $("#<portlet:namespace />campoTexto").val();
		
		Liferay.fire('enviarDatos', {nombre: nombre, texto1: 'texto fijo', campo2: 'otro texto'} );
	});

	// Envio IPC via Sesion
	miformulario.one('#<portlet:namespace />addButtonSesion').on('click', function(event) {
		console.log("Enviando formulario sesion...");
		miformulario.attr("action", '${enviarSesionUrl}');
		miformulario.submit();
	});

	// Envio IPC via evento (No debiera llegar al otro portlet)
	miformulario.one('#<portlet:namespace />addButtonEvent').on('click', function(event) {
		console.log("Enviando otro formulario...");
		AUI().io.request('${otroEventoUrl}', {
			method: 'POST',
			form: {id: '<portlet:namespace />fm'},
			on: {
				failure: function() { console.log("Ocurrio un error..."); },
				success: function(event, id, obj) { console.log("Formulario enviado!."); }
			}
		});
	});

</aui:script>
