package com.ghost.prueba.ipc.sender.portlet;

import com.ghost.prueba.ipc.sender.constants.PortletSenderPortletKeys;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.ParamUtil;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletSession;
import javax.portlet.ProcessAction;
import javax.xml.namespace.QName;

import org.osgi.service.component.annotations.Component;

/**
 * @author Francisco Mendoza C.
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=Demo IPC Sender",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + PortletSenderPortletKeys.PortletSender,
		"javax.portlet.resource-bundle=content.Language",
        "com.liferay.portlet.private-session-attributes=false",
        "javax.portlet.supported-publishing-event=crearEvento;https://ghost.tinet.cl/events",
        "javax.portlet.supported-publishing-event=otroEventoAca;https://ghost.tinet.cl/events",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class PortletSenderPortlet extends MVCPortlet {

	private static final Log LOGGER = LogFactoryUtil.getLog(PortletSenderPortlet.class);

	private static final String QNAME = "https://ghost.tinet.cl/events";
	
    @ProcessAction(name = "addEvento")
    public void addEvento(ActionRequest request, ActionResponse response) {

        // Enviar datos del evento a recibir
		final String nombre = ParamUtil.getString(request, "campoTexto");

        LOGGER.info("Valor a agregar: " + nombre);

        QName qname = new QName(QNAME, "crearEvento");
        LOGGER.info("Llamada: " + nombre);        
        response.setEvent(qname, nombre);
        
        // Enviar datos a otro evento (No se debe procesar).
        QName qname2 = new QName(QNAME, "otroEventoAca");
        response.setEvent(qname2, "Valor que no debe ver");
    }

    @ProcessAction(name = "enviarSesion")
    public void enviarSesion(ActionRequest request, ActionResponse response) {
		final String nombre = ParamUtil.getString(request, "campoTexto");

    	LOGGER.info("Valor a enviar a la sesion: " + nombre);
        PortletSession sesion = request.getPortletSession();
        sesion.setAttribute("nombre", nombre, PortletSession.APPLICATION_SCOPE);
        LOGGER.info("Dato enviado a sesion.");
    }
    
    @ProcessAction(name = "otroEvento")
    public void otroEvento(ActionRequest request, ActionResponse response) {
    	LOGGER.info("Invocar otro evento.");
        // Enviar datos a otro evento. (No debe leerlo el otro portlet).
        QName qname2 = new QName(QNAME, "otroEventoAca");
        response.setEvent(qname2, "Valor que no debe ver");
    }
}