package com.ghost.prueba.ipc.receiver.portlet;

import com.ghost.prueba.ipc.receiver.constants.PortletReceiverPortletKeys;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import java.io.IOException;

import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

/**
 * @author Francisco Mendoza C.
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + PortletReceiverPortletKeys.PortletReceiver,
		"javax.portlet.resource-bundle=content.Language",
		"com.liferay.portlet.private-session-attributes=false",
        "javax.portlet.supported-processing-event=crearEvento;https://ghost.tinet.cl/events",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class PortletReceiverPortlet extends MVCPortlet {

	private static final Log LOGGER = LogFactoryUtil.getLog(PortletReceiverPortlet.class);
	
	private static final String EVENTO = "eventoRender";

	@Override
	public void processEvent(EventRequest request, EventResponse response) throws PortletException, IOException {
		Event event = request.getEvent();
		String eventValue = "";
		
		LOGGER.info("Evento recibido: " + event.getName());

		if (event.getName().equals("crearEvento")) {
			LOGGER.info("Evento encontrado!");
			eventValue = (String) event.getValue();
			LOGGER.info("Valor desde eventoIPC: " + eventValue);
		}
		
		request.setAttribute(EVENTO, eventValue);
		
		super.processEvent(request, response);
	}

	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {

		PortletSession sesion = renderRequest.getPortletSession();
		String nombre = (String) sesion.getAttribute("nombre", PortletSession.APPLICATION_SCOPE);

		LOGGER.info("Valor desde la sesion: " + nombre);

		renderRequest.setAttribute("nombreSesion", nombre);

		// Borrar despues de usar el atributo.
		// sesion.removeAttribute("nombre", PortletSession.APPLICATION_SCOPE);

		super.doView(renderRequest, renderResponse);
	}
}
