<%@ include file="/init.jsp" %>

<c:set var="valorEvento" value="${eventoRender}" />

<p>
	<b><liferay-ui:message key="portletreceiver.caption"/></b>
	<b>-- ${valorEvento} --</b>
</p>
<aui:form>

    <!-- Recibir el valor desde el otro portlet via POST -->
    <aui:input name="campoEvento" type="text" label="Evento" value="${eventoRender}"/>

    <!-- Recibir el valor con el "set" de arriba -->
    <aui:input name="campoEvento2" type="text" label="Evento2" value="${valorEvento}"/>

    <!-- Usado por el script "Liferay.on Evento" -->
    <aui:input name="campo" type="text" label="recibido ajax" />

    <aui:input name="campo2" type="text" label="recibido sesion" value="${nombreSesion}"/>
</aui:form>

<aui:script>

    // Script para recibir el evento via JS desde el otro portlet.
    Liferay.on('enviarDatos', function(event) {
        item = $("#<portlet:namespace />campo")

        console.log("recibido: " + event.nombre);
        
        item.val(event.nombre + " - " + event.texto1 + " - " + event.campo2);
    });
</aui:script>
