# Ejemplo de comunicación entre portlets

Para generar la comunicación entre _portlets_ se utilizan principalmente 3 métodos:

## 1. Via Eventos

Para comunicar _portlets_ utilizando eventos se debe configurar:

### En Portlet Sender

- Una propiedad indicando que el _portlet_ tiene soporte para publicar eventos, con un identificador del evento y un namespace: _`crearEvento`_ y _`https://ghost.tinet.cl/events`_:

```java
    "javax.portlet.supported-publishing-event=crearEvento;https://ghost.tinet.cl/events",
```

- Enviar el parámetro en alguna parte del controller, en esta parte se utiliza el mismo identificador _`crearEvento`_ y namespace: _`https://ghost.tinet.cl/events`_:

```java
    QName qname = new QName("https://ghost.tinet.cl/events", "crearEvento");
    LOGGER.info("Llamada: " + nombre);
    response.setEvent(qname, nombre);
```

### En Portlet Receiver

- La misma propiedad para soportar la publicación de eventos usando el mismo identificador y namespace: _`crearEvento`_ y _`https://ghost.tinet.cl/events`_:

```java
    "javax.portlet.supported-publishing-event=crearEvento;https://ghost.tinet.cl/events",
```

- Crear un método que recibe el parámetro de nombre __processEvent__ o un método con otro nombre con la anotación _processEvent_:

```java
    @Override
    public void processEvent(EventRequest request, EventResponse response) throws PortletException, IOException {
        Event event = request.getEvent();
        String eventValue = "";

        LOGGER.info("Evento recibido: " + event.getName());

        if (event.getName().equals("crearEvento")) {
            LOGGER.info("Evento encontrado!");
            eventValue = (String) event.getValue();
            LOGGER.info("Valor desde eventoIPC: " + eventValue);
        }

        request.setAttribute(EVENTO, eventValue);

        super.processEvent(request, response);
    }
```

---------------------

## 2. Via ajax

Para comunicar _portlets_ utilizando _ajax_ se debe configurar:

### En Portlet Sender

- Se debe configurar un evento _`Liferay.fire`_ en la pagina jsp. Este método recibe un identificador del evento: _`enviarDatos`_ y un objeto con los datos a enviar: _{nombre: nombre, texto1: 'texto fijo', campo2: 'otro texto'}_.

```javascript
    Liferay.fire('enviarDatos', {nombre: nombre, texto1: 'texto fijo', campo2: 'otro texto'} );
```

### En el Portlet Receiver

- Se debe configurar un receptor del evento en la pagina jsp.

```html
<aui:script>
    Liferay.on('enviarDatos', function(event) {
        // aca existen los valores:
        // event.nombre, event.texto1, event.campo2
    });
</aui:script>
```

---------------------

## 3. Via sesión

Para comunicar _portlets_ utilizando sesiones se debe configurar:

### En Portlet Sender

- Se debe configurar permisos en el componente para definir los atributos de sesión como públicos.

```java
    "com.liferay.portlet.private-session-attributes=false",
```

- Con ese configuración se puede definir el atributo en la sesión, en este caso se define un atributo de nombre _nombre_:

```java
    PortletSession sesion = request.getPortletSession();
    sesion.setAttribute("nombre", nombre, PortletSession.APPLICATION_SCOPE);
```

### En Portlet Receiver

- Se debe configurar el mismo permiso del componente:

```java
    "com.liferay.portlet.private-session-attributes=false",
```

- Ahora se puede obtener el atributo desde la sesión:

```java
    PortletSession sesion = renderRequest.getPortletSession();
    String nombre = (String) sesion.getAttribute("nombre", PortletSession.APPLICATION_SCOPE);
```

---------------------

## Ejemplo práctico

Vamos a generar una ejemplo de comunicación entre _portlets_, para esto generaremos 2 _portlets_. Esto se realiza con el siguiente comando:

```console
blade create -t mvc-portlet -p com.ghost.prueba.ipc.sender -c PortletSender ipc-sender
blade create -t mvc-portlet -p com.ghost.prueba.ipc.receiver -c PortletReceiver ipc-receiver
```

Una vez creados los _portlets_ se debe configurar cada uno:

__ipc-sender: PortletSenderPortlet.java__

```java
package com.ghost.prueba.ipc.sender.portlet;

import com.ghost.prueba.ipc.sender.constants.PortletSenderPortletKeys;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.ParamUtil;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletSession;
import javax.portlet.ProcessAction;
import javax.xml.namespace.QName;

import org.osgi.service.component.annotations.Component;

/**
 * @author Francisco Mendoza C.
 */
@Component(
    immediate = true,
    property = {
        "com.liferay.portlet.display-category=category.sample",
        "com.liferay.portlet.instanceable=true",
        "javax.portlet.display-name=Demo IPC Sender",
        "javax.portlet.init-param.template-path=/",
        "javax.portlet.init-param.view-template=/view.jsp",
        "javax.portlet.name=" + PortletSenderPortletKeys.PortletSender,
        "javax.portlet.resource-bundle=content.Language",
        "com.liferay.portlet.private-session-attributes=false",
        "javax.portlet.supported-publishing-event=crearEvento;https://ghost.tinet.cl/events",
        "javax.portlet.supported-publishing-event=otroEventoAca;https://ghost.tinet.cl/events",
        "javax.portlet.security-role-ref=power-user,user"
    },
    service = Portlet.class
)
public class PortletSenderPortlet extends MVCPortlet {

    private static final Log LOGGER = LogFactoryUtil.getLog(PortletSenderPortlet.class);

    private static final String QNAME = "https://ghost.tinet.cl/events";

    @ProcessAction(name = "addEvento")
    public void addEvento(ActionRequest request, ActionResponse response) {

        // Enviar datos del evento a recibir
        final String nombre = ParamUtil.getString(request, "campoTexto");

        LOGGER.info("Valor a agregar: " + nombre);

        QName qname = new QName(QNAME, "crearEvento");
        LOGGER.info("Llamada: " + nombre);
        response.setEvent(qname, nombre);

        // Enviar datos a otro evento (No se debe procesar).
        QName qname2 = new QName(QNAME, "otroEventoAca");
        response.setEvent(qname2, "Valor que no debe ver");
    }

    @ProcessAction(name = "enviarSesion")
    public void enviarSesion(ActionRequest request, ActionResponse response) {
        final String nombre = ParamUtil.getString(request, "campoTexto");

        LOGGER.info("Valor a enviar a la sesion: " + nombre);
        PortletSession sesion = request.getPortletSession();
        sesion.setAttribute("nombre", nombre, PortletSession.APPLICATION_SCOPE);
        LOGGER.info("Dato enviado a sesion.");
    }

    @ProcessAction(name = "otroEvento")
    public void otroEvento(ActionRequest request, ActionResponse response) {
        LOGGER.info("Invocar otro evento.");
        // Enviar datos a otro evento. (No debe leerlo el otro portlet).
        QName qname2 = new QName(QNAME, "otroEventoAca");
        response.setEvent(qname2, "Valor que no debe ver");
    }
}
```

__ipc-sender: view.jsp__

```html
<%@ page language="java" contentType="text/html; charset=UTF-8" %>

<%@ include file="./init.jsp" %>

<portlet:actionURL name="addEvento" var="addEventoUrl"/>
<portlet:actionURL name="otroEvento" var="otroEventoUrl"/>
<portlet:actionURL name="enviarSesion" var="enviarSesionUrl"/>

<aui:form action="${addEventoUrl}">
    <aui:input name="campoTexto" type="text" label="Texto a enviar" />
    <aui:button name="addButton" type="button" value="Enviar via POST" />
    <aui:button name="addButtonJS" type="button" value="Enviar via JS" />
    <aui:button name="addButtonSesion" type="button" value="Enviar via Sesion" />
    <aui:button name="addButtonEvent" type="button" value="Enviar otro evento" />
</aui:form>


<aui:script use="aui-base,aui-request">

    var miformulario = AUI().one('#<portlet:namespace />fm');

    // Envio IPC via evento.
    // Se debe hacer por POST para que se refresque el otro formulario.
    miformulario.one('#<portlet:namespace />addButton').on('click', function(event) {
        console.log("Enviando formulario...");
        miformulario.attr('action', '${addEventoUrl}');
        miformulario.submit();
    });

    // Envio IPC via JS
    miformulario.one('#<portlet:namespace />addButtonJS').on('click', function(event) {
        console.log("Enviar datos al otro portlet");
        //recuperar texto
        nombre = $("#<portlet:namespace />campoTexto").val();

        Liferay.fire('enviarDatos', {nombre: nombre, texto1: 'texto fijo', campo2: 'otro texto'} );
    });

    // Envio IPC via Sesion
    miformulario.one('#<portlet:namespace />addButtonSesion').on('click', function(event) {
        console.log("Enviando formulario sesion...");
        miformulario.attr("action", '${enviarSesionUrl}');
        miformulario.submit();
    });

    // Envio IPC via evento (No debiera llegar al otro portlet)
    miformulario.one('#<portlet:namespace />addButtonEvent').on('click', function(event) {
        console.log("Enviando otro formulario...");
        AUI().io.request('${otroEventoUrl}', {
            method: 'POST',
            form: {id: '<portlet:namespace />fm'},
            on: {
                failure: function() { console.log("Ocurrio un error..."); },
                success: function(event, id, obj) { console.log("Formulario enviado!."); }
            }
        });
    });

</aui:script>
```

__ipc-receiver: PortletReceiverPortlet.java__

```java
package com.ghost.prueba.ipc.receiver.portlet;

import com.ghost.prueba.ipc.receiver.constants.PortletReceiverPortletKeys;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import java.io.IOException;

import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

/**
 * @author Francisco Mendoza C.
 */
@Component(
    immediate = true,
    property = {
        "com.liferay.portlet.display-category=category.sample",
        "com.liferay.portlet.instanceable=true",
        "javax.portlet.init-param.template-path=/",
        "javax.portlet.init-param.view-template=/view.jsp",
        "javax.portlet.name=" + PortletReceiverPortletKeys.PortletReceiver,
        "javax.portlet.resource-bundle=content.Language",
        "com.liferay.portlet.private-session-attributes=false",
        "javax.portlet.supported-processing-event=crearEvento;https://ghost.tinet.cl/events",
        "javax.portlet.security-role-ref=power-user,user"
    },
    service = Portlet.class
)
public class PortletReceiverPortlet extends MVCPortlet {

    private static final Log LOGGER = LogFactoryUtil.getLog(PortletReceiverPortlet.class);

    private static final String EVENTO = "eventoRender";

    @Override
    public void processEvent(EventRequest request, EventResponse response) throws PortletException, IOException {
        Event event = request.getEvent();
        String eventValue = "";

        LOGGER.info("Evento recibido: " + event.getName());

        if (event.getName().equals("crearEvento")) {
            LOGGER.info("Evento encontrado!");
            eventValue = (String) event.getValue();
            LOGGER.info("Valor desde eventoIPC: " + eventValue);
        }

        request.setAttribute(EVENTO, eventValue);

        super.processEvent(request, response);
    }

    @Override
    public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
            throws IOException, PortletException {

        PortletSession sesion = renderRequest.getPortletSession();
        String nombre = (String) sesion.getAttribute("nombre", PortletSession.APPLICATION_SCOPE);

        LOGGER.info("Valor desde la sesion: " + nombre);

        renderRequest.setAttribute("nombreSesion", nombre);

        // Borrar despues de usar el atributo.
        // sesion.removeAttribute("nombre", PortletSession.APPLICATION_SCOPE);

        super.doView(renderRequest, renderResponse);
    }
}
```

__ipc-receiver: view.jsp__

```html
<%@ include file="/init.jsp" %>

<c:set var="valorEvento" value="${eventoRender}" />

<p>
    <b><liferay-ui:message key="portletreceiver.caption"/></b>
    <b>-- ${valorEvento} --</b>
</p>
<aui:form>

    <!-- Recibir el valor desde el otro portlet via POST -->
    <aui:input name="campoEvento" type="text" label="Evento" value="${eventoRender}"/>

    <!-- Recibir el valor con el "set" de arriba -->
    <aui:input name="campoEvento2" type="text" label="Evento2" value="${valorEvento}"/>

    <!-- Usado por el script "Liferay.on Evento" -->
    <aui:input name="campo" type="text" label="recibido ajax" />

    <aui:input name="campo2" type="text" label="recibido sesion" value="${nombreSesion}"/>
</aui:form>

<aui:script>

    // Script para recibir el evento via JS desde el otro portlet.
    Liferay.on('enviarDatos', function(event) {
        item = $("#<portlet:namespace />campo")

        console.log("recibido: " + event.nombre);

        item.val(event.nombre + " - " + event.texto1 + " - " + event.campo2);
    });
</aui:script>
 ```

Seguidamente ejecutar el comando:

```console
blade deploy
```

En Liferay se debe crear una pagina para poder desplegar los _portlets_, para este caso se escogió una _pagina de widget_ con _2 columnas_.

```console
estructura > Paginas > nuevo
    pagina publica
    pagina de widget
    2 columnas.
```

Agregar _portlets_ en pagina nueva y probar.
